import requests
import json
from contextlib import suppress


while True:

    count = 0

    dilSecimi = input ("\nHangi dilden çevireceğinizi seçiniz [(İ)ngilizce/(T)ürkçe]:(çıkış için 'enter') ")
    
    if dilSecimi == "":
        print ("Exit by user")
        exit()

    word = input ("Çevirilmesini istediğiniz kelime: ")
    

    dilSecimi_lower = dilSecimi.lower()
    word_lower = word.lower()
    word_lower_nospace = word_lower.replace(" ", "%20")

    api = (f"http://api.tureng.com/v1/dictionary/entr/{word_lower_nospace}")

    response = requests.get(api)

    data = response.text

    parsed = json.loads(data)

    isFound = parsed["IsFound"]

    if str(isFound) == "True":


        if dilSecimi_lower == ("i" or "ingilizce"):
            
            print (f"\n{(word_lower).title()} kelimesinin İngilizce karşılıkları :")
            a = str(parsed["AResults"])
            for i in a:
                if i == "e":
                    count +=1
            b = range(0,count)
            for c in b:
                with suppress(Exception):
                    türkçesi = parsed["AResults"][int(c)]["TermB"]
                    print (türkçesi)
            

        elif dilSecimi_lower == ("t" or "türkçe"):
         
            print (f"\n{(word_lower).title()} kelimesinin İngilizce karşılıkları :")
            a = str(parsed["BResults"])
            for i in a:
                if i == "e":
                    count +=1
            b = range(0,count)
            for c in b:
                with suppress(Exception):
                    ingilizcesi = parsed["BResults"][int(c)]["TermA"]
                    print (ingilizcesi)
    else:
        öneriler = parsed["Suggestions"]

        print ("\nAradığınız kelime bulunamadı. Bunu mu demek istediniz:")
        print (f"\n{öneriler}")


    print ("--------------------------------------------------")
